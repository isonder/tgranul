# Preparation of Raw Data for Analysis

Preparation steps are made in several steps:

- **step 0:**  
  Convert raw data into a memory friendly, easy to load format.
- **step 1:**  
  Apply basic filter(s) that remove nasty phenomena which could not be prevented in the lab.