# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.16.1
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %% [markdown]
# # Grims2.0-12

# %% [markdown]
# Collect raw data and organize it into a suitable ASDF structure where it makes sense.
#
# **ATTENTION**  
# *This will create new asdf files from scratch and potentially delete existing additions that were
# already made.*
#
# Experiment date: 2012-07-03

# %%
# %matplotlib inline
import numpy as np
import matplotlib.pyplot as plt
import asdf
import pandas as pd
from pathlib import Path

from bismarckload import load_meta

# %% [markdown]
# ## Load Raw Data

# %% [markdown]
# ### Metadata

# %%
md = load_meta('../../data/2.0/2012-07-03_Grims2.0-12.log')
md

# %%
SRATE = md['sampling rate']
SRATE

# %% [markdown]
# ### Main Data

# %%
data = pd.read_csv(
    '../../data/2.0/2012-07-03_Grims2.0-12.txt',
    sep='\t', names=['pold', 'p09', 'p10', 'p11'], index_col=0, skiprows=2
)
data.index.name = 't'
# Re-generate time line, to fix the 1e5 sample bug
data.index = pd.Index(data=np.arange(0.0, len(data) / SRATE, 1 / SRATE), name='t')
# data.loc[:, :] *= 1e5

data.head()

# %%
fig, axs = plt.subplots(nrows=4, sharex=True, constrained_layout=True)

for ax, col in zip(axs, data.columns):
    ax.plot(data.loc[:, col], label=col)
axs[-1].set(
    xlabel=r'$t\ /\ \mathrm{s}$'
)
fig.set_size_inches(8, 10)

# %% [markdown]
# ## Save to `asdf`

# %% [markdown]
# ### Create a "Toplevel" File

# %%
md['name']

# %%
afname = f'../../data/2.0/asdf/{md["name"]}.asdf'
afname

# %%
af = asdf.AsdfFile(uri=afname)

# %%
af['raw'] = {}
raw = af['raw']
raw['metadata'] = md

# %% [markdown]
# ### Separate File for Each Sensor

# %% [markdown]
# Save each sensor log into a separate file and create a reference entry in the toplevel thing

# %%
auri = Path(af.uri)
# time column
afr = asdf.AsdfFile(uri=auri.parent / f"{auri.stem}_raw-time{auri.suffix}")
afr.tree = {'experiment': md['name'], 'date': md['date'],
            'srate': SRATE, 'unit': 's', 'time': data.index.values}
afr.write_to(afr.uri, all_array_storage='internal', all_array_compression='zlib')
raw['time'] = {'$ref': f"{afr.uri.name}#time"}

# pressure
for col, name in zip(data.columns, md['names']):
    afr = asdf.AsdfFile(uri=auri.parent / f"{auri.stem}_raw-{name}{auri.suffix}")
    afr.tree = {
        'experiment': md['name'], 'date': md['date'], 'srate': SRATE,
        'unit': 'bar', name: data.loc[:, col].values
    }
    afr.write_to(afr.uri, all_array_compression='zlib')
    raw[name] = {'$ref': f"{afr.uri.name}#{name}"}

# %% [markdown]
# ### Add a History Entry

# %%
af.add_history_entry(
    description="Add raw data and raw metadata.\n"
    "- Elements written:\n"
    "  - 'raw/metadata'\n"
    "  - 'raw/data'\n"
    "- Program source: 'anl/prep_grims2.0-12.ipynb'",
    software={
        "name": "Bismarck Analysis Stack.",
        "author": "The PVL Team",
        "homepage": "https://to.do",
        "version": "0.1"
    }
)

# %% [markdown]
# ### Write Out and Close

# %%
af.find_references()

# %%
af.info(max_rows=50)

# %%
af.write_to(af.uri)

# %%
af.close()

# %%
del data

# %% [markdown]
# ### Re-Open and Check Data

# %%
af = asdf.open('../../data/2.0/asdf/Grims2.0-12.asdf', mode='r')

# %%
af.find_references()

# %%
af['raw']['time'][:10]

# %%
af['raw']['KISTLERold'][:10]

# %%
fig, axs = plt.subplots(nrows=4, sharex=True, constrained_layout=True)

for ax, col in zip(axs, ["KISTLERold", "KISTLER09", "KISTLER10", "KISTLER11"]):
    ax.plot(af['raw']['time'][:], af['raw'][col][:], label=col)
axs[-1].set(
    xlabel=r'$t\ /\ \mathrm{s}$'
)
fig.set_size_inches(8, 10)
