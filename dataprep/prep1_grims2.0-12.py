# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.16.1
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %%
# %matplotlib inline
# # %matplotlib widget
import matplotlib.pyplot as plt
import numpy as np
import asdf
import scipy.signal as sig

# %%
from pathlib import Path

# %%
af = asdf.open('../../data/2.0/asdf/Grims2.0-12.asdf', mode='rw')
af.info()

# %%
md = af['raw']['metadata']
md

# %%
SRATE = md['sampling rate']
SRATE

# %%
t = af['raw']['time'][:]

# %%
p = af['raw']['KISTLERold'][:]

# %%
fig, ax = plt.subplots()

ax.plot(t, p)

# %% [markdown]
# ## Set Up a Highpass Filter

# %% [markdown]
# This makes an FIR highpass with a Kaiser window, with a 89dB stop band and 20 Hz cutoff.

# %%
ln, beta = sig.kaiserord(89, 20 * 2 / SRATE)
ln, beta

# %% [markdown]
# Filter window in seconds

# %%
ln / SRATE

# %% [markdown]
# Filter's impulse response

# %%
taps = sig.firwin(ln, 10 * 2 / SRATE, window=('kaiser', beta), pass_zero=False)

# %% [markdown]
# Get the filter's frequency response

# %%
f, h = sig.freqz(taps, worN=len(t), fs=SRATE)

# %% [markdown]
# ### Plot the Filter Characteristics

# %%
ln2 = ln // 2
tau = (np.arange(len(taps)) - ln2) / SRATE
ln2, tau[[0, -1]]

# %%
idx = np.zeros(len(taps), dtype=bool)
idx[:ln2 - 10:600] = True
idx[ln2 + 10::600] = True
idx[ln2 - 10:ln2 + 10] = True

# %%
idx2 = f < 100
idx3 = idx2.copy()
idx3[[len(idx2[idx2]), -1]] = True

# %%
fig, axd = plt.subplot_mosaic(
    [['impulse', 'impulse', 'impin', 'impin'],
     ['impulse', 'impulse', 'impin', 'impin'],
     ['impulse', 'impulse', 'freqin1', 'freqin1'],
     ['freqres', 'freqres', 'freqin1', 'freqin1'],
     ['freqres', 'freqres', 'freqin2', 'freqin2'],
     ['freqres', 'freqres', 'freqin2', 'freqin2']],
    constrained_layout=True
)

ax1 = axd['impulse']
ax1.plot(tau[idx], taps[idx])
ax1.set(
    ylabel='$h\ /\ \mathrm{s^{-1}}$', xlabel=r'$\tau\ /\ \mathrm{s}$'
)
ax4 = axd['impin']
ax4.plot(tau[idx], taps[idx])
ax4.set(
    ylim=(-6e-6, 1e-6),
    xlabel=r'$\tau\ /\ \mathrm{s}$', ylabel=r'$h$'
)
ax2 = axd['freqres']
ax2.plot(f[idx3],  np.abs(h[idx3]))
# ax2.plot(f[~idx][[0, -1]], np.abs(h[~idx][[0, -1]]))
ax2.set(
    xscale='log', yscale='log',
    xlabel=r'$f\ /\ \mathrm{Hz}$', ylabel=r'$H$'
)
ax3 = axd['freqin1']
ax3.plot(f[idx2], np.abs(h[idx2]))
ax3.set(
    xscale='log', yscale='log',
    xlabel=r'$f\ /\ \mathrm{Hz}$', ylabel=r'$H$',
    xlim=(None, 20), ylim=(5e-5, .5)
)
ax4 = axd['freqin2']
ax4.plot(f[idx2], np.abs(h[idx2]))
ax4.set(
    xscale='log',
    # yscale='log',
    xlabel=r'$f\ /\ \mathrm{Hz}$', ylabel=r'$H$',
    xlim=(15, 100), ylim=(1 - 2e-4, 1 + 2e-4)
)
fig.set_size_inches(10, 8)

# %% [markdown]
# ## Filter the Pressure Signals

# %% [markdown]
# ### `KISTLERold`, `p0`

# %%
pf = sig.fftconvolve(p, taps, mode='same')

# %%
fig, (ax1, ax2) = plt.subplots(nrows=2, constrained_layout=True)

ax1.plot(t, p, label='raw signal')
ax1.plot(t, pf, label='filtered signal')
ax1.set(
    xlabel=r'$t\ /\ \mathrm{s}$', ylabel=r'$p$',
    xlim=(0, 6)
)
ax1.legend(loc='upper right')
idx = np.logical_and(t > 1.603e-2, t < 1.630e-2)
ax2.plot(t[idx], p[idx], label='raw signal')
ax2.plot(t[idx], pf[idx], label='filtered signal')
ax2.set(
    xlabel=r'$t\ /\ \mathrm{s}$', ylabel=r'$p$',
    xlim=t[idx][[0, -1]]
)
ax2.legend(loc='upper right')
fig.set_size_inches(10, 8)

# %%
af['pressures'] = {}
press = af['pressures']

# %%
paf = asdf.AsdfFile(
    uri=str(Path(af.uri[5:]).parent / f"{md['name']}_pressures-p0.asdf")
)

# %%
paf['date'] = md['date']
paf['experiment'] = md['name']
paf['srate'] = SRATE
paf['unit'] = 'Pa'
paf['start time'] = 0.0
paf['p0'] = p * 1e5

# %%
paf.info()

# %%
paf.write_to(paf.uri, all_array_compression='zlib')
paf.close()

# %%
press['p0'] = {'$ref': f"{md['name']}_pressures-p0.asdf#p0"}

# %% [markdown]
# ### `KISTLER09`, `p1`

# %%
p = af['raw']['KISTLER09'][:]

# %%
pf = sig.fftconvolve(p, taps, mode='same')

# %%
fig, (ax1, ax2) = plt.subplots(nrows=2, constrained_layout=True)

ax1.plot(t, p, label='raw signal')
ax1.plot(t, pf, label='filtered signal')
ax1.set(
    xlabel=r'$t\ /\ \mathrm{s}$', ylabel=r'$p$',
    xlim=(0, 6)
)
ax1.legend(loc='upper right')
idx = np.logical_and(t > 2.5665, t < 2.56675)
ax2.plot(t[idx], p[idx], label='raw signal')
ax2.plot(t[idx], pf[idx], label='filtered signal')
ax2.set(
    xlabel=r'$t\ /\ \mathrm{s}$', ylabel=r'$p$',
    xlim=t[idx][[0, -1]]
)
ax2.legend(loc='upper right')
fig.set_size_inches(10, 8)

# %%
paf = asdf.AsdfFile(
    uri=str(Path(af.uri[5:]).parent / f"{md['name']}_pressures-p1.asdf")
)

# %%
paf['date'] = md['date']
paf['experiment'] = md['name']
paf['srate'] = SRATE
paf['unit'] = 'Pa'
paf['start time'] = 0.0
paf['p1'] = p * 1e5

# %%
paf.info()

# %%
paf.write_to(paf.uri, all_array_compression='zlib')
paf.close()

# %%
press['p1'] = {'$ref': f"{md['name']}_pressures-p1.asdf#p1"}

# %% [markdown]
# ### `KISTLER11`, `p3`

# %%
p = af['raw']['KISTLER11'][:]

# %%
pf = sig.fftconvolve(p, taps, mode='same')

# %%
fig, (ax1, ax2) = plt.subplots(nrows=2, constrained_layout=True)

ax1.plot(t, p, label='raw signal')
ax1.plot(t, pf, label='filtered signal')
ax1.set(
    xlabel=r'$t\ /\ \mathrm{s}$', ylabel=r'$p$',
    xlim=(0, 6)
)
ax1.legend(loc='upper right')
idx = np.logical_and(t > 0.72845, t < 0.72870)
ax2.plot(t[idx], p[idx], label='raw signal')
ax2.plot(t[idx], pf[idx], label='filtered signal')
ax2.set(
    xlabel=r'$t\ /\ \mathrm{s}$', ylabel=r'$p$',
    xlim=t[idx][[0, -1]]
)
ax2.legend(loc='upper right')
fig.set_size_inches(10, 8)

# %%
paf = asdf.AsdfFile(
    uri=str(Path(af.uri[5:]).parent / f"{md['name']}_pressures-p3.asdf")
)

# %%
paf['date'] = md['date']
paf['experiment'] = md['name']
paf['srate'] = SRATE
paf['unit'] = 'Pa'
paf['start time'] = 0.0
paf['p1'] = p * 1e5

# %%
paf.info()

# %%
paf.write_to(paf.uri, all_array_compression='zlib')
paf.close()

# %%
press['p3'] = {'$ref': f"{md['name']}_pressures-p3.asdf#p3"}

# %%
af.add_history_entry(
    description="Apply a basic filter to raw pressures and store it.\n"
    "- Elements written:\n"
    "  - 'pressures'\n"
    "- Program source: 'anl/prep1_grims2.0-12.ipynb'",
    software={
        "name": "Bismarck Analysis Stack.",
        "author": "The PVL Team",
        "homepage": "https://to.do",
        "version": "0.1"
    }
)

# %%
af.find_references()
# af.info(max_rows=50)

# %%
af.write_to(af.uri)
af.close()
