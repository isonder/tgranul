from datetime import datetime
from configparser import ConfigParser
from pathlib import Path
from typing import Union

pth_or_str = Union[Path, str]


def parse_channels(chnstr: str) -> list:
    return chnstr.split(',')


def parse_name(namestr: str) -> tuple:
    dtstr, name = namestr.split("_")
    return name, datetime.fromisoformat(dtstr)


def parse_trigger(trgstr: str) -> bool:
    return True if trgstr.lower == "true" else False


def parse_chnlnames(namestr: str) -> list:
    return namestr.split(',')


def parse_scales(scalestr: str) -> list:
    return scalestr.split(',')


reg = {
    "device": lambda v: v,
    "channels": parse_channels,
    "sampling rate": lambda v: float(v),
    "name": parse_name,
    "trigger": parse_trigger,
    "slope": lambda v: v,
    "trigger_level": lambda v: float(v),
    "trigger_source": lambda v: v,
    "pretrigger": lambda v: float(v),
    "samples": lambda v: int(float(v)),
    "names": parse_chnlnames,
    "scales": parse_scales
}


def load_meta(fname: pth_or_str) -> dict:
    """Parse PVL logger metadata into a dictionary.
    """
    fname = Path(fname)
    assert fname.exists(), f"Cannot find file {str(fname)}."
    cnfp = ConfigParser()
    cnfp.read(filenames=[fname])
    md = {}
    for k, v in cnfp['loggerconf'].items():
        v = v.strip('"')
        if v.endswith(r"\0D\0A"):
            v = v[:-6]
        if k == "name":
            v = reg[k](v)
            md['name'] = v[0]
            md["date"] = v[1]
        else:
            md[k] = reg[k](v)
    return md.copy()
